<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S05 Activity: Client-Server Communications</title>
</head>

<body>
    <form method="POST">
        Email: <input type="text" name="email" required>
        <br>
        Password: <input type="password" name="password" required>
        <br>
        <button type="submit">
            Login
        </button>
        <br>
        <?php
        session_start();
        if (isset($_POST['email']) && isset($_POST['password'])){
            if ($_POST['email'] === 'johnsmith@gmail.com' && $_POST['password'] === '1234') {
                header('Location:server.php');
            } else {
                echo "You have entered invalid use name or password";
            }
        }
        ?>
    </form>
</body>

</html>